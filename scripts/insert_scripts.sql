-- Заполнение таблицы "clients"
INSERT INTO rpg_shops_database.clients (name, type)
VALUES
('Шир', 'Государство'), ('Асгард', 'Государство'),
('Атлантида', 'Государство'), ('Вестерос', 'Государство'),
('Корусант', 'Государство'), ('Римский союз', 'Государство'),
('Артур', 'Физическое лицо'), ('Геральд', 'Физическое лицо'),
('Макиавелли', 'Физическое лицо'), ('Харальд Суровый', 'Физическое лицо'),
('Роберт Лукавый', 'Физическое лицо'), ('Гарольд Кроткий', 'Физическое лицо'),
('Фу Хао', 'Физическое лицо'), ('Артимизия Первая', 'Физическое лицо'),
('Лозен', 'Физическое лицо'), ('Накано Такеко', 'Физическое лицо'),
('Александр Македонский', 'Физическое лицо'), ('Зенобия', 'Физическое лицо'),
('Остиндская Торговая Компания', 'Юридическое лицо'), ('Единное собрание торговцев', 'Юридическое лицо'),
('Средеземье Экспресс', 'Юридическое лицо'), ('Тульские Оружейники', 'Юридическое лицо'),
('Объединение семи торговцев', 'Юридическое лицо'), ('Черные перебежчики', 'Юридическое лицо'),
('Добрые Циркачи', 'Юридическое лицо'), ('Палачи', 'Юридическое лицо'),
('Церковь второго', 'Юридическое лицо'), ('Гильдия Пивоваров', 'Юридическое лицо'),
('Единные союз крестьян', 'Юридическое лицо'), ('Пахотные земли', 'Юридическое лицо');

-- Заполнение таблицы "products"
INSERT INTO rpg_shops_database.products (name, product_type, cost)
VALUES
('Эль Фей', 'Напитки', '5'), ('Двуручный оленний меч', 'Оружие', '15420'),
('Ручной арболет', 'Оружие', '11290'), ('Тяжелый арболет', 'Оружие', '16250'),
('Булова варвора', 'Оружие', '550'), ('Экскалибур', 'Оружие', '180000'),
('Эльфийский лук', 'Оружие', '14000'), ('Ржавый меч', 'Оружие', '1300'),
('Меч новичка', 'Оружие', '1600'), ('Стальной одноручный меч', 'Оружие', '4500'),
('Топор берсерка', 'Оружие', '2000'), ('Трупка с дротиками', 'Оружие', '440'),
('Катана Акамусм', 'Оружие', '4800'), ('Катана Кацука', 'Оружие', '2300'),
('Железный шлем', 'Броня', '1890'), ('Железный нагрудник', 'Броня', '10420'),
('Набор кожанной брони', 'Броня', '2300'), ('Черный кольчужный плащ', 'Броня', '4800'),
('Адамантидовый бронник', 'Броня', '49890'), ('Железные наколенники', 'Броня', '1400'),
('Железные перчатки', 'Броня', '2500'), ('Изумрудный шлем', 'Броня', '98000'),
('Кольчуга', 'Броня', '3980'), ('Кожанный подшлемник', 'Броня', '900'),
('Индийская кольчуга', 'Броня', '4800'), ('Железный доспех низкого качества', 'Броня', '5400'),
('Абракадабрус', 'Артифакт', '80000'), ('Атлас бесконечных горизонтов', 'Артифакт', '14000'),
('Амулет щитосража', 'Артифакт', '54000'), ('Бесконечный гримуар', 'Артифакт', '540000'),
('Астральный осколок', 'Артифакт', '5000'), ('Гамак миров', 'Артифакт', '900000'),
('Движимая магией рука', 'Артифакт', '89000'), ('Заполярные сапоги', 'Артифакт', '54000'),
('Куб силового поля', 'Артифакт', '9800'), ('Ножницы отсекающие тени', 'Артифакт', '41000'),
('Портальный компас', 'Артифакт', '1000'), ('Призрачный фонарь', 'Артифакт', '4900'),
('Сыр раф', 'Еда', '2'), ('Нож', 'Оружие', '30');

-- Заполнение таблицы "shops"
INSERT INTO rpg_shops_database.shops (name, country, city, address)
VALUES
('Таверна-Магазин Красный Лось', 'Средеземье', 'Тир', 'Среднее Звено 15'), ('Джентельмен', 'Средеземье', 'Варварград', 'Морская 1'),
('Оружейная лавочка у Двордворфича', 'Средеземье', 'Шахтсбург', 'Пещера 4'), ('20 тысячь лье под водой', 'Атлантида', 'Атлантида', 'Деререворубово 21'),
('Черный восход', 'Ривия', 'Ривия', 'Сертухово 41'), ('Солнечное утро', 'Нильфгаард', 'Аттре', 'Сэра Артура 123'),
('Пруд пруди', 'Темерия', 'Тавсбург', 'Персикова улица 40'), ('Сказочное место', 'Цинтра', 'Колотушкина', 'Пушкина 902'),
('Красный клен', 'Редания', 'Вендардар', 'Воорхис 24'), ('Бардовское место', 'Моровинд', 'Малое Отрадное', 'Цирилла 50'),

('Малая Сова', 'Кенинсберг', 'Велипово', 'Канта 109'), ('Мираж', 'Пустынные дали', 'Центральная зона', 'Оливье 404'),
('Малая Лавка', 'Империя Ревил', 'Ревил', 'Центральная 1'), ('Лавка Мира', 'Восток', 'Западный город', 'Деререворубово 21'),
('Пятнацатая миля', 'Морское государство', 'Митрополитен', 'Сараворова 80'), ('Артос', 'Республика Женева', 'Жарков', 'Мебельдорская 30'),
('Гранатовый магазин', 'Норвилд', 'Ликинсберг', 'Улица павшего ангела 61'), ('Бережливый покупатель', 'Филилис', 'Червик', 'Хордовая 129'),
('La Farse', 'Галдендон', 'Вондерморт', 'Ламарк 910'), ('Бебебе', 'Франсиское государство', 'Парилиж', 'Башневая 2190');

-- Заполнение таблицы "stocks"
INSERT INTO rpg_shops_database.stocks (stock_id, product_id, shop_id)
VALUES
(
        1,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Эль Фей'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Средеземье'
            AND city = 'Тир'
            AND address = 'Среднее Звено 15'
        )
),
(
        1,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Сыр раф'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Средеземье'
            AND city = 'Тир'
            AND address = 'Среднее Звено 15'
        )
),
(
        2,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Ручной арболет'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Средеземье'
            AND city = 'Варварград'
            AND address = 'Морская 1'
        )
),
(
        2,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Тяжелый арболет'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Средеземье'
            AND city = 'Варварград'
            AND address = 'Морская 1'
        )
),
(
        3,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Двуручный оленний меч'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Средеземье'
            AND city = 'Шахтсбург'
            AND address = 'Пещера 4'
        )
),
(
        3,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Булова варвора'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Средеземье'
            AND city = 'Шахтсбург'
            AND address = 'Пещера 4'
        )
),
(
        4,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Экскалибур'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Атлантида'
            AND city = 'Атлантида'
            AND address = 'Деререворубово 21'
        )
),
(
        4,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Эльфийский лук'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Атлантида'
            AND city = 'Атлантида'
            AND address = 'Деререворубово 21'
        )
),
(
        5,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Ржавый меч'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Ривия'
            AND city = 'Ривия'
            AND address = 'Сертухово 41'
        )
),
(
        5,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Меч новичка'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Ривия'
            AND city = 'Ривия'
            AND address = 'Сертухово 41'
        )
),
(
        6,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Стальной одноручный меч'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Нильфгаард'
            AND city = 'Аттре'
            AND address = 'Сэра Артура 123'
        )
),
(
        6,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Топор берсерка'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Нильфгаард'
            AND city = 'Аттре'
            AND address = 'Сэра Артура 123'
        )
),
(
        7,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Трупка с дротиками'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Темерия'
            AND city = 'Тавсбург'
            AND address = 'Персикова улица 40'
        )
),
(
        7,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Катана Акамусм'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Темерия'
            AND city = 'Тавсбург'
            AND address = 'Персикова улица 40'
        )
),
(
        8,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Катана Кацука'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Цинтра'
            AND city = 'Колотушкина'
            AND address = 'Пушкина 902'
        )
),
(
        8,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Железный шлем'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Цинтра'
            AND city = 'Колотушкина'
            AND address = 'Пушкина 902'
        )
),
(
        9,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Железный нагрудник'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Редания'
            AND city = 'Вендардар'
            AND address = 'Воорхис 24'
        )
),
(
        9,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Набор кожанной брони'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Редания'
            AND city = 'Вендардар'
            AND address = 'Воорхис 24'
        )
),
(
        10,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Черный кольчужный плащ'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Моровинд'
            AND city = 'Малое Отрадное'
            AND address = 'Цирилла 50'
        )
),
(
        10,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Адамантидовый бронник'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Моровинд'
            AND city = 'Малое Отрадное'
            AND address = 'Цирилла 50'
        )
),
(
        11,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Железные наколенники'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Кенинсберг'
            AND city = 'Велипово'
            AND address = 'Канта 109'
        )
),
(
        11,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Железные перчатки'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Кенинсберг'
            AND city = 'Велипово'
            AND address = 'Канта 109'
        )
),
(
        12,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Изумрудный шлем'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Морское государство'
            AND city = 'Митрополитен'
            AND address = 'Сараворова 80'
        )
),
(
        12,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Кольчуга'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Морское государство'
            AND city = 'Митрополитен'
            AND address = 'Сараворова 80'
        )
),
(
        13,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Кожанный подшлемник'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Восток'
            AND city = 'Западный город'
            AND address = 'Деререворубово 21'
        )
),
(
        13,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Индийская кольчуга'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Восток'
            AND city = 'Западный город'
            AND address = 'Деререворубово 21'
        )
),
(
        14,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Железный доспех низкого качества'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Империя Ревил'
            AND city = 'Ревил'
            AND address = 'Центральная 1'
        )
),
(
        14,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Абракадабрус'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Империя Ревил'
            AND city = 'Ревил'
            AND address = 'Центральная 1'
        )
),
(
        15,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Атлас бесконечных горизонтов'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Пустынные дали'
            AND city = 'Центральная зона'
            AND address = 'Оливье 404'
        )
),
(
        15,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Амулет щитосража'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Пустынные дали'
            AND city = 'Центральная зона'
            AND address = 'Оливье 404'
        )
),
(
        16,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Бесконечный гримуар'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Франсиское государство'
            AND city = 'Парилиж'
            AND address = 'Башневая 2190'
        )
),
(
        16,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Астральный осколок'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Франсиское государство'
            AND city = 'Парилиж'
            AND address = 'Башневая 2190'
        )
),
(
        17,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Гамак миров'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Галдендон'
            AND city = 'Вондерморт'
            AND address = 'Ламарк 910'
        )
),
(
        17,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Движимая магией рука'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Галдендон'
            AND city = 'Вондерморт'
            AND address = 'Ламарк 910'
        )
),
(
        18,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Заполярные сапоги'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Филилис'
            AND city = 'Червик'
            AND address = 'Хордовая 129'
        )
),
(
        18,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Куб силового поля'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Филилис'
            AND city = 'Червик'
            AND address = 'Хордовая 129'
        )
),
(
        19,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Ножницы отсекающие тени'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Норвилд'
            AND city = 'Ликинсберг'
            AND address = 'Улица павшего ангела 61'
        )
),
(
        19,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Портальный компас'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Норвилд'
            AND city = 'Ликинсберг'
            AND address = 'Улица павшего ангела 61'
        )
),
(
        20,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Призрачный фонарь'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Республика Женева'
            AND city = 'Жарков'
            AND address = 'Мебельдорская 30'
        )
),
(
        20,
        (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE name = 'Нож'
        ),
        (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE country = 'Республика Женева'
            AND city = 'Жарков'
            AND address = 'Мебельдорская 30'
        )
);

-- Заполнение таблицы "orders"
INSERT INTO rpg_shops_database.orders (client_id, order_data)
VALUES
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE name = 'Шир'
    ),
    '2020-02-02'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 1
    ),
    '2011-10-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 4
    ),
    '2010-07-02'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 3
    ),
    '2013-11-11'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 4
    ),
    '2020-03-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 1
    ),
    '2006-05-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 6
    ),
    '2017-11-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 5
    ),
    '2010-04-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 5
    ),
    '2016-12-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 7
    ),
    '2019-02-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 8
    ),
    '2020-03-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 8
    ),
    '2012-06-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 9
    ),
    '2007-02-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 10
    ),
    '2002-06-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 11
    ),
    '2013-07-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 13
    ),
    '2005-03-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 18
    ),
    '2014-01-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 21
    ),
    '2018-06-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 14
    ),
    '2001-06-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 28
    ),
    '2004-04-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 19
    ),
    '2002-02-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 19
    ),
    '2010-06-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 25
    ),
    '2002-05-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 4
    ),
    '2010-03-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 26
    ),
    '2002-03-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 25
    ),
    '2019-09-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 13
    ),
    '2009-04-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 12
    ),
    '2009-05-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 22
    ),
    '2007-03-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 14
    ),
    '1980-11-01'
),
(
    (
    SELECT client_id
    FROM rpg_shops_database.clients
    WHERE client_id = 3
    ),
    '1820-01-01'
);
-- Заполнение таблицы "employees"
INSERT INTO rpg_shops_database.employees (name, surname, department, job_title, salary, shop_id)
VALUES
(
    'Nikolo',
    'Makiavelli',
    'Администратор',
    'Старший администратор',
    1900,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 1
    )
),
(
    'Nina',
    'Grill',
    'продавец',
    'Старший продавец',
    1590,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 2
    )
),
(
    'Billy',
    'Bob',
    'продавец',
    'Младший продавец',
    0,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 3
    )
),
(
    'Sara',
    'Smith',
    'продавец',
    'Старший продавец',
    550,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 4
    )
),
(
    'Jonny',
    'Dep',
    'продавец',
    'Младший продавец',
    800,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 5
    )
),
(
    'Lucius',
    'Arakskiy',
    'продавец',
    'Младший продавец',
    900,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 6
    )
),
(
    'Ivan',
    'Big head',
    'продавец',
    'Старший продавец',
    1300,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 7
    )
),
(
    'Din',
    'Waterball',
    'продавец',
    'Младший продавец',
    800,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 8
    )
),
(
    'Lilia',
    'Kravchuk',
    'продавец',
    'Младший продавец',
    100,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 9
    )
),
(
    'Arethos',
    'Lilivisiy',
    'продавец',
    'Младший продавец',
    1000,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 10
    )
),
(
    'Mir',
    'Mar',
    'продавец',
    'Младший продавец',
    890,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 11
    )
),
(
    'Simple',
    'Sanya',
    'продавец',
    'Младший продавец',
    750,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 12
    )
),
(
    'Vivaldi',
    'Great',
    'продавец',
    'Старший продавец',
    1800,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 13
    )
),
(
    'Motsart',
    'White',
    'продавец',
    'Младший продавец',
    1000,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 14
    )
),
(
    'Mimir',
    'Marar',
    'продавец',
    'Старший продавец',
    1500,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 15
    )
),
(
    'Gandalf',
    'Grey',
    'Мастер',
    'Хранитель артифактов',
    1432,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 2
    )
),
(
    'Aragorn',
    'Son of Arathorn',
    'администратор',
    'Младший администратор',
    1765,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 3
    )
),
(
    'Hermione',
    'Granger',
    'продавец',
    'Бронник',
    1219,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 1
    )
),
(
    'Legolas',
    'Greenleaf',
    'мастер',
    'Оружейник',
    1956,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 6
    )
),
(
    'Gimli',
    'Son of Gloin',
    'стражник',
    'Бронник',
    1673,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 4
    )
),
(
    'Frodo',
    'Baggins',
    'мастер',
    'Оружейник',
    1024,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 11
    )
),
(
    'Jon',
    'Snow',
    'мастер',
    'Оружейник',
    1387,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 13
    )
),
(
    'Arya',
    'Stark',
    'продавец',
    'Старший продавец',
    1825,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 5
    )
),
(
    'Bilbo',
    'Baggins',
    'стражник',
    'Глава стражи',
    1156,
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 8
    )
);
-- Заполнение таблицы "order_x_products"
INSERT INTO rpg_shops_database.order_x_products (order_id, product_id, shop_id)
VALUES
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 1
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 1
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 1
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 2
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 40
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 3
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 2
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 3
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 5
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 3
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 2
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 17
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 4
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 12
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 18
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 5
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 14
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 19
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 5
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 22
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 8
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 6
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 39
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 13
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 7
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 24
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 13
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 8
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 32
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 12
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 9
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 4
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 11
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 10
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 8
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 10
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 11
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 15
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 9
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 11
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 21
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 8
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 12
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 24
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 7
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 13
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 3
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 6
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 15
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 7
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 5
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 16
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 8
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 4
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 17
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 9
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 2
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 18
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 4
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 3
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 19
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 9
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 3
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 20
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 11
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 2
    )
),
(
    (
        SELECT order_id
        FROM rpg_shops_database.orders
        WHERE order_id = 21
    ),
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 37
    ),
    (
        SELECT shop_id
        FROM rpg_shops_database.shops
        WHERE shop_id = 1
    )
);
-- Заполнение таблицы "weapons"
INSERT INTO rpg_shops_database.weapons (product_id, weapon_type, damage, provider)
VALUES
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 2
    ),
    'Двуручный меч',
    40,
    'Средиземный охотник'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 3
    ),
    'Арбалет',
    8,
    'Мастерская Стрела'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 4
    ),
    'Арбалет',
    12,
    'Мастерская Стрела'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 5
    ),
    'Булава',
    13,
    'Воин Эрех'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 6
    ),
    'Двуручный меч',
    45,
    'Разбойник Эрен'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 7
    ),
    'Лук',
    20,
    'Эльф Зихбур'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 8
    ),
    'Одноручный меч',
    11,
    'Воин Эрих'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 9
    ),
    'Одноручный меч',
    14,
    'Торговая компания Ланберга'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 10
    ),
    'Одноручный меч',
    18,
    'Торговая компания Ланберга'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 11
    ),
    'Топор',
    14,
    'Воин Гатс'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 12
    ),
    'Трубка',
    4,
    'Авантюрист Де Гама'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 13
    ),
    'Двуручный меч',
    24,
    'Дом мечников Рюдзо'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 14
    ),
    'Двуручный меч',
    18,
    'Дом мечников Рюдзо'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 40
    ),
    'Нож',
    0,
    'Торговая компания все для дома'
);

-- Заполнение таблицы "armors"
INSERT INTO rpg_shops_database.armors (product_id, armor_type, protection, provider)
VALUES
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 15
    ),
    'Шлем',
    8,
    'Кузница у Кузьмича'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 16
    ),
    'Нагрудник',
    18,
    'Кузница у Кузьмича'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 17
    ),
    'Компект',
    15,
    'Мануфактура Кубрар'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 18
    ),
    'Плащ',
    14,
    'Кузница у Кузьмича'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 19
    ),
    'Нагрудник',
    32,
    'Королевская мастерская Ривии'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 20
    ),
    'Наколенники',
    8,
    'Кузница у Кузьмича'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 21
    ),
    'Перчатки',
    9,
    'Кузница у Кузьмича'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 22
    ),
    'Шлем',
    13,
    'Королевская мастерская Ривии'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 23
    ),
    'Колчуга',
    14,
    'Кузница у Кузьмича'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 24
    ),
    'Подшлемник',
    1,
    'Мануфактура Кубрар'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 25
    ),
    'Колчуга',
    16,
    'Авантюрист Бербери'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 26
    ),
    'Бронник',
    13,
    'Авантюрист Бейбери'
);

-- Заполнение таблицы "artifacts";
INSERT INTO rpg_shops_database.artifacts (product_id, description)
VALUES
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 27
    ),
    'Сундук, в котором время от времени появляются ингредиент для зелий, стоимостью менее 1 зм'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 28
    ),
    'Бесконечнай карта, на которой появляется окржающие ее ландшафт'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 29
    ),
    'Амулет, способный призвать щитсража'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 30
    ),
    'Гримуар, дающий своему обладателю усиление магических способностей. Кроме того, в нем случайным образом появляются заклинания'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 31
    ),
    'Оскол с астрального плана'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 32
    ),
    'Способен открыть портал между 2 точками на выбор'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 33
    ),
    'Магический протез, который можно присоеденить к любой части тела'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 34
    ),
    'Сапоги с подогревом'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 35
    ),
    'Куб, при активации которого создается магическое поле'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 36
    ),
    'Ножница, способные отсекать тень'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 37
    ),
    'Компас, указывающий на последний портал, через который он прошел'
),
(
    (
        SELECT product_id
        FROM rpg_shops_database.products
        WHERE product_id = 38
    ),
    'Фонарь с заключенным в нем духом'
);
