-- Удаление схемы
DROP SCHEMA IF EXISTS rpg_shops_database CASCADE;
-- Создание таблиц

-- Создание схемы базы данных магазина из RPG
CREATE SCHEMA rpg_shops_database;

-- Информация о клиентах
CREATE TABLE rpg_shops_database.clients (
    client_id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    type TEXT DEFAULT 'Физическое лицо'
);

-- Информация о заказе от клиетна
CREATE TABLE rpg_shops_database.orders (
    order_id SERIAL PRIMARY KEY,
    client_id SERIAL NOT NULL,
    order_data DATE DEFAULT (NOW()),
    FOREIGN KEY (client_id)
    REFERENCES rpg_shops_database.clients (client_id)
);

-- Информация о продуктах
CREATE TABLE rpg_shops_database.products (
    product_id SERIAL PRIMARY KEY,
    name TEXT,
    product_type TEXT,
    cost MONEY
);

-- Информация о магазине
CREATE TABLE rpg_shops_database.shops (
    shop_id SERIAL PRIMARY KEY,
    name TEXT,
    country TEXT NOT NULL,
    city TEXT NOT NULL,
    address TEXT NOT NULL
);

-- Информация о складе
CREATE TABLE rpg_shops_database.stocks (
    stock_id SERIAL NOT NULL,
    product_id SERIAL NOT NULL,
	shop_id SERIAL NOT NULL,
    PRIMARY KEY (stock_id, product_id, shop_id),
    FOREIGN KEY (product_id)
    REFERENCES rpg_shops_database.products (product_id),
	FOREIGN KEY (shop_id)
    REFERENCES rpg_shops_database.products (product_id)
);

-- Таблица-связка "Заказ-продукт"
CREATE TABLE rpg_shops_database.order_x_products (
    order_id SERIAL NOT NULL,
    product_id SERIAL NOT NULL,
    shop_id SERIAL NOT NULL,
    PRIMARY KEY (order_id, product_id, shop_id),
    FOREIGN KEY (order_id)
    REFERENCES rpg_shops_database.orders (order_id),
    FOREIGN KEY (product_id)
    REFERENCES rpg_shops_database.products (product_id),
    FOREIGN KEY (shop_id)
    REFERENCES rpg_shops_database.shops (shop_id)
);

-- Информация о сотрудниках
CREATE TABLE rpg_shops_database.employees (
    employee_id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    surname TEXT NOT NULL,
    department TEXT NOT NULL,
    job_title TEXT NOT NULL,
    salary INTEGER,
    shop_id SERIAL NOT NULL,
    FOREIGN KEY (shop_id)
    REFERENCES rpg_shops_database.shops (shop_id)
);

-- Информация о оружии
CREATE TABLE rpg_shops_database.weapons (
    product_id SERIAL NOT NULL,
    weapon_type TEXT,
    damage INTEGER,
    provider TEXT,
    PRIMARY KEY (product_id),
    FOREIGN KEY (product_id)
    REFERENCES rpg_shops_database.products (product_id)
);

-- Информация о броне
CREATE TABLE rpg_shops_database.armors (
    product_id SERIAL NOT NULL,
    armor_type TEXT,
    protection INTEGER,
    provider TEXT,
    PRIMARY KEY (product_id),
    FOREIGN KEY (product_id)
    REFERENCES rpg_shops_database.products (product_id)
);

-- Информация о артефактах
CREATE TABLE rpg_shops_database.artifacts (
    product_id SERIAL NOT NULL,
    description TEXT,
    PRIMARY KEY (product_id)
);

-- Добавление внешнего ключа через ALTER для разнообразия
ALTER TABLE rpg_shops_database.artifacts
ADD FOREIGN KEY (product_id)
REFERENCES rpg_shops_database.products (product_id)
DEFERRABLE INITIALLY DEFERRED;