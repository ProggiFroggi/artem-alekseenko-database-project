# Курсовой проект

Подробнее об оценивании и организационных моментах в
[репозитории курса](https://github.com/destitutiones/hsse-db-2024/blob/main/formal/project_description.md).

## Проверяющая система

Для упрощения проверки проекта вам предлагается самостоятельно в gitlab CI
настроить запуск скриптов по созданию, наполнению таблиц, запуску к ним запросов
и пр. Это позволит проверять корректность скриптов без ручного его запуска вами
и проверяющими.

_Примечание:_ здесь тестирование (обязательные его этапы) заключается именно в
запуске скрипта и последующем выводе результатов, а не в проверке корректности
результата посредством сравнения с какими-то "правильными значениями".

## Структура проекта

**Если хочется лучше понять, что происходит с технической точки зрения, можно
обратиться к [документации](https://docs.gitlab.com/ee/ci/) по gitlab CI.**

Данный репозиторий – пример того, как должен выглядеть ваш проект. Здесь уже
приведен пример конфигурационного файла для запуска CI и некоторых запускаемых
sql-скриптов.

### Примерная структура

```shell
.
├── .gitlab-ci.yml           <- описание пайплайна проекта (по шагам)
├── README.md
├── docs                     <- схемы ваших моделей
│   ├── README.md            <- описание идеи вашего проекта и структуры репозитория, если изменяете её
│   ├── conceptual-model.png
│   ├── logical-model.png
├── requirements.txt         <- зависимости для Python
├── scripts                  <- sql-скрипты (структура этой директории может быть другой на ваше усмотрение)
│   ├── inserts1.sql
│   ├── ...
│   ├── table1_ddl.sql
│   └── ...
├── tests                    <- директория с тестами (структура этой директории может быть другой на ваше усмотрение)
│   ├── __init__.py
│   ├── conftest.py
│   ├── test1.py
│   ├── test2.py
│   └── ...
└── ...                      <- любые другие необходимые файлы

```

В файле `.gitlab-ci.yml` приведено много комментариев, уточняющих, как именно
работает пайплайн.

### Как перенести это на свой проект?

Опустив вопрос красоты и полноты, опишем, как сделать так, чтобы CI запускался:

1. Создаем репозиторий для своего проекта в Gitlab. Либо публичный, либо
   добавляем своего семинариста, чтобы у того был доступ к коду.
2. Заводим **обязательно** как минимум следующую структуру:

```shell
.
├── .gitlab-ci.yml           <- полностью копируем
├── README.md
├── requirements.txt         <- полностью копируем
├── scripts                  <- сюда складываем свои sql-скрипты по нужной итерации
├── utils                    <- полностью копируем
```

3. В `.gitlab-ci.yml` в разделе `ddl scripts/stages` прописываем свои команды
   запуска sql-скриптов из директории scripts.
4. После того как запушим код в репозиторий, увидим запустившийся пайплайн:
   <img src='./img/img1.png' width='600'>
5. При клике на диаграмму загрузки сможем увидеть все стейджи пайплайна и их
   статус: <img src='./img/img2.png' width='600'>
6. "Провалившись" в конкретный стейдж, увидим консоль запуска и вывод указанных
   нами в `.gitlab-ci.yml` команд. Там же при наличии ошибок увидим лог, по
   которому будем разбираться с причиной проблем.

   <img src='./img/img3.png' width='600'>

## Кодстайл

Ознакомиться с автоматизированным запуском линтеров при помощи `pre-commit` вы
можете, посмотрев
[лекцию по MLOps](https://www.youtube.com/watch?v=8v0ZX92AE6c). В предшествующих
лекциях этого же курса также подробнее рассказывается про идею CI/CD.

Для того чтобы не перегружать пример проекта лишней информацией, приведем
пример, как можно форматировать sql-скрипты в репозитории при помощи линтера.
Это не обязательная активность, но желательная для тех, кто хочет хранить в
проекте аккуратный код.

В консоли из корня репозитория запускаем следующие команды:

1. Создаем окружение (чтобы только в рамках него устанавливать линтер),
   активируем его.

```bash
python3 -m venv test_env
source test_env/bin/activate
```

2. Скачиваем `sqlfluff`

```bash
python3 -m pip install --upgrade sqlfluff
```

3. Запускаем линтер для скриптов из папки `./scripts`. Папку указывать очень
   желательно, т.к. иначе линтер может начать проверять скрипты во всех прочих
   sql-файлах, расположенных в директории запуска или ниже.

```bash
sqlfluff lint --dialect postgres ./scripts
```

4. Последний шаг желательно запускать перед каждым коммитом новых скриптов в
   репозиторий, чтобы сохранять там отформатированную версию. Если хотите
   узнать, как делать это автоматизированно, советую обратиться к лекции в
   начале пункта.

